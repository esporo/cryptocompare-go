package tests

import (
	"testing"

	"bitbucket.org/esporo/cryptocurrency-go/cryptocompare-go/cryptocompare"

	"github.com/stretchr/testify/require"
)

func TestTopPair(t *testing.T) {
	cc, err := cryptocompare.NewClient(nil)
	require.NoError(t, err)

	btc, err := cc.TopPair("BTC", 10)
	require.NoError(t, err)
	require.NotEmpty(t, btc)
	require.Equal(t, len(btc.Data), 10)
	require.Equal(t, btc.Response, "Success")

	btc, err = cc.TopPair("", 5)
	require.Equal(t, len(btc.Data), 0)
	require.Equal(t, btc.Response, "Error")
}
