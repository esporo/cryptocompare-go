package cryptocompare

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

// Client CryptoCompare API Client
type Client struct {
	BaseURL     *url.URL
	rateLimiter RateLimiter
	//	HTTPClient *http.Client
	//	Websocket *ccWebsocket
}

// NewClient creates and initializes
func NewClient(rateLimiter RateLimiter) (*Client, error) {

	c := &Client{rateLimiter: rateLimiter}
	if c.rateLimiter == nil {
		c.rateLimiter = defaultRTLimiter
	}
	var err error
	c.BaseURL, err = url.Parse("https://min-api.cryptocompare.com")
	return c, err
}

//Get top pairs by volume for a currency (always uses our aggregated data). The number of pairs you get is the minimum of the limit you set (default 5) and the total number of pairs available
func (c *Client) TopPair(fsym string, limit int) (*TopPair, error) {
	//нужно проверять на корректность входные данные или тупо отправлять в API?
	httpClient := http.Client{}

	reqURL := c.BaseURL
	reqURL.Path = "data/top/pairs"

	values := reqURL.Query()
	values.Set("fsym", fsym)
	values.Set("limit", strconv.Itoa(limit))
	reqURL.RawQuery = values.Encode()

	log.Println(reqURL.String())
	c.rateLimiter.WaitForRequest()

	resp, err := httpClient.Get(reqURL.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	topPair := TopPair{}
	err = json.Unmarshal(body, &topPair)
	if err != nil {
		return nil, err
	}

	return &topPair, nil
}

//Returns all the exchanges that CryptoCompare has integrated with.
func (c *Client) AllExchange() (*AllExchange, error) {
	httpClient := http.Client{}

	reqURL := c.BaseURL
	reqURL.Path = "data/all/exchanges"
	c.rateLimiter.WaitForRequest()

	resp, err := httpClient.Get(reqURL.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var allExchange AllExchange
	err = json.Unmarshal(body, &allExchange)
	if err != nil {
		return nil, err
	}
	return &allExchange, nil
}

//Get general info for all the coins available on the website
func (c *Client) CoinList() (*CoinList, error) {
	httpClient := http.Client{}

	reqURL, err := url.Parse("https://www.cryptocompare.com/api/data/coinlist/")
	if err != nil {
		return nil, err
	}
	c.rateLimiter.WaitForRequest()
	resp, err := httpClient.Get(reqURL.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var coinList CoinList
	err = json.Unmarshal(body, &coinList)
	if err != nil {
		return nil, err
	}
	return &coinList, nil
}
