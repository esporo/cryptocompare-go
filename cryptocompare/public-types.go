package cryptocompare

// top pairs by volume for a currency
type TopPair struct {
	Response string `json:"Response"`
	Data     []struct {
		Exchange    string  `json:"exchange"`
		FromSymbol  string  `json:"fromSymbol"`
		ToSymbol    string  `json:"toSymbol"`
		Volume24H   float64 `json:"volume24h"`
		Volume24HTo float64 `json:"volume24hTo"`
	} `json:"Data"`
}

type CoinInfo map[string][]string
type AllExchange map[string]CoinInfo

type CoinListDataInfo struct {
	ID                  string `json:"Id"`
	URL                 string `json:"Url"`
	ImageURL            string `json:"ImageUrl"`
	Name                string `json:"Name"`
	Symbol              string `json:"Symbol"`
	CoinName            string `json:"CoinName"`
	FullName            string `json:"FullName"`
	Algorithm           string `json:"Algorithm"`
	ProofType           string `json:"ProofType"`
	FullyPremined       string `json:"FullyPremined"`
	TotalCoinSupply     string `json:"TotalCoinSupply"`
	PreMinedValue       string `json:"PreMinedValue"`
	TotalCoinsFreeFloat string `json:"TotalCoinsFreeFloat"`
	SortOrder           string `json:"SortOrder"`
	Sponsored           bool   `json:"Sponsored"`
}

type CoinListData map[string]CoinListDataInfo

type CoinList struct {
	Response         string `json:"Response"`
	Message          string `json:"Message"`
	BaseImageURL     string `json:"BaseImageUrl"`
	BaseLinkURL      string `json:"BaseLinkUrl"`
	DefaultWatchlist struct {
		CoinIs    string `json:"CoinIs"`
		Sponsored string `json:"Sponsored"`
	} `json:"DefaultWatchlist"`
	Data CoinListData `json:"Data"`
	Type int          `json:"Type"`
}
