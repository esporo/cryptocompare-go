package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/esporo/cryptocurrency-go/cryptocompare-go/cryptocompare"
)

func main() {
	cc, err := cryptocompare.NewClient(nil)

	/*
		//--TopPair
		btc, err := cc.TopPair("BTC", 1)
		log.Println("btc = ", btc)
		log.Println("err = ", err)
	*/

	/*
		//--AllExchange
		allExchange, err := cc.AllExchange()
		if err != nil {
			log.Fatalln(err)
		}
		len := len(*allExchange)
		log.Println(len)
		i := 0
		allMarketsArr := make([]string, len)
		for key := range *allExchange {
			allMarketsArr[i] = key
			i++
		}
		log.Println("allMarketsArr = ", allMarketsArr)

		data, err := json.Marshal(allMarketsArr)
		if err != nil {
			log.Fatalln(err)
		}

		err = ioutil.WriteFile("allMarketsArr.json", data, os.ModeAppend)
		if err != nil {
			log.Fatalln(err)
		}
	*/

	//CoinList
	coinList, err := cc.CoinList()
	if err != nil {
		log.Fatalln(err)
	}

	len := len((*coinList).Data)
	log.Println(len)

	i := 0
	coinNameList := make([]string, len)
	for key := range (*coinList).Data {
		coinNameList[i] = key
		i++
	}
	log.Println("coinNameList = ", coinNameList)

	data, err := json.Marshal(coinNameList)
	if err != nil {
		log.Fatalln(err)
	}

	err = ioutil.WriteFile("coinNameList.json", data, os.ModeAppend)
	if err != nil {
		log.Fatalln(err)
	}

}
